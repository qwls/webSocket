package com.websocket.service;

import com.websocket.utils.WebsocketUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;

/**
 * @author qinwei
 * @date 2024/11/02 周六 16:48
 * @description
 */
@Component
@ServerEndpoint(value = "/chat/{userId}")
@Slf4j
public class WebsocketService {

    /**
     * 连接事件，加入注解
     * @param userId
     * @param session
     */
    @OnOpen
    public void onOpen(@PathParam(value = "userId") String userId, Session session) {
        //权限校验
//        System.out.println("Authorization:" + session.getUserProperties().get("Authorization"));
//        if (StringUtils.isEmpty(session.getUserProperties().get("Authorization"))) {
//            throw new RuntimeException("Login Fail");
//        }
        log.info("WebSocket连接成功，用户ID: " + userId);
        // 添加到session的映射关系中
        WebsocketUtil.addSession(userId, session);
        // 广播通知，某用户上线了
//        WebsocketUtil.sendMessageForAll(message);
    }

    /**
     * 连接事件，加入注解
     * 用户断开链接
     *
     * @param userId
     * @param session
     */
    @OnClose
    public void onClose(@PathParam(value = "userId") String userId, Session session) {
        log.info("WebSocket连接断开，用户ID: " + userId);
        // 删除映射关系
        WebsocketUtil.removeSession(userId);
        // 广播通知，用户下线了
//        WebsocketUtil.sendMessageForAll(message);
    }

    /**
     * 当接收到用户上传的消息
     *
     * @param userId
     * @param session
     */
    @OnMessage
    public void onMessage(@PathParam(value = "userId") String userId, Session session, String message) {
        log.info("用户ID: " + userId + " 发送消息: " + message);
        // 直接广播
//        WebsocketUtil.sendMessageForAll(msg);
    }

    /**
     * 处理用户活连接异常
     *
     * @param session
     * @param throwable
     */
    @OnError
    public void onError(Session session, Throwable throwable) {
        log.info("用户异常断开链接，原因： " + throwable.getMessage());
        try {
            session.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        throwable.printStackTrace();
    }

}
