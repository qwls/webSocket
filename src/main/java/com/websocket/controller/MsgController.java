package com.websocket.controller;

import com.websocket.utils.WebsocketUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

/**
 * @author qinwei
 * @date 2024/11/02 周六 16:51
 * @description
 */
@RestController
@RequestMapping("/msg")
@Slf4j
public class MsgController {

    @PostMapping("/send")
    public void send(@RequestParam("id") String id, @RequestParam("message") String message) {
        log.info("发送消息给：" + id + "-" + message);
        WebsocketUtil.sendMessage(id, message);
    }

    @PostMapping("/sendAll")
    public void sendAll(@RequestParam("message") String message) {
        log.info("群发消息：" + message);
        WebsocketUtil.sendMessageForAll(message);
    }

    @PostMapping("/sendUserList")
    public void sendAll(@RequestParam("userIds") Set<String> userIds, @RequestParam("message") String message) {
        log.info("发送多人消息：" + userIds.toString() + message);
        WebsocketUtil.sendMessageForAll(message);
    }
}
