//package com.websocket.web;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.servlet.ModelAndView;
//
//import java.io.IOException;
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * Created with IDEA
// * author:QinWei
// * Date:2019/10/17   0017
// * Time:14:23
// */
//
//@Controller
//public class CheckCenterController {
//    //页面请求
//    @GetMapping("/index/{userId}")
//    public ModelAndView socket(@PathVariable String userId) {
//        ModelAndView mav = new ModelAndView("/index");
//        mav.addObject("userId", userId);
//        return mav;
//    }
//
//    @Autowired
//    private WebSocketServer webSocket;
//
//    @RequestMapping("sendInfo")
//    @ResponseBody
//    public String sendInfo(@RequestParam String msg) {
//        try {
//            webSocket.sendInfo(msg);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return "信息发送异常!";
//        }
//
//        return "发送成功~";
//    }
//}