package com.websocket.job;

import com.websocket.service.WebsocketService;
import com.websocket.utils.WebsocketUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created with IDEA
 * author:QinWei
 * Date:2019/10/18   0018
 * Time:11:14
 */
@Component
public class JobService {


    @Autowired
    private WebsocketService websocketService;

    /**
     *
     * @throws IOException
     * 定时任务，后台发送前台信息，属于定时推送
     */
    @Scheduled(cron = "0/10 * * * * ?")
    public void printTime() throws IOException {
//        System.out.println("current time :"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
//        WebsocketUtil.sendMessageForAll("定时发起每10秒一次");

    }

}
